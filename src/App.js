import FormContainer from './Components/FormContainer.jsx'
import React, { Component } from 'react'
import { Container } from 'react-bootstrap'
import Header from './Components/Header'

export class App extends Component {
    render() {
        return (
            <Container>
                <Header />
                <div className="container-wrap ">
                    <div className="wrap-form-app">
                        <FormContainer />
                    </div>
                </div>
            </Container>
        )
    }
}

export default App
