import { SUBMIT_FORM } from './FormType'

let initialState = {
    userInputs: [
        { inputType: 'text', label: 'Name', name: 'name', userInput: '' },
        { inputType: 'text', label: 'Email', name: 'email', userInput: '' },
        {
            inputType: 'password',
            label: 'Password',
            name: 'password',
            userInput: '',
        },
        {
            inputType: 'radio',
            label: 'Gender',
            name: 'gender',
            values: ['Male', 'Female'],
            userInput: '',
        },
        {
            inputType: 'checkbox',
            label: 'Vehicle',
            name: 'vehicle',
            values: ['Bike', 'Car'],
            userInput: '',
        },
        {
            inputType: 'dropdown',
            label: 'Vehicle Type',
            name: 'vehicle_type',
            value: [
                'Hatchback',
                'Sedan',
                'SUV',
                'Road bikes',
                'Mountain bikes',
            ],
            userInput: '',
        },
        {
            inputType: 'textarea',
            label: 'Address',
            name: 'address',
            values: '',
            userInput: '',
        },
    ],
}

const formReducer = (state = initialState, action) => {
    try {
        switch (action.type) {
            case SUBMIT_FORM: {
                return {
                    ...state,
                    userInputs: action.payload,
                }
            }
            default:
                return { ...state }
        }
    } catch (err) {
        console.error(err)
    }
}

export default formReducer
