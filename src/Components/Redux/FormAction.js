import { SUBMIT_FORM } from './FormType.js'

export const submitForm = (formValues = {}) => {
    return {
        type: SUBMIT_FORM,
        payload: formValues,
    }
}
