import { createStore } from 'redux'
import formReducer from './FormReducer.js'

const store = createStore(formReducer)

export default store
