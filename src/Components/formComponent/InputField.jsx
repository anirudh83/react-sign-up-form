import React, { Component } from 'react'
import { Form } from 'react-bootstrap'
import validator from 'validator'
import '../Css/style.css'

export class InputField extends Component {
    state = { error: '' }

    checkEmail = (email) => {
        if (validator.isEmpty(email)) {
            this.setState({ error: 'Can not empty' })
        } else if (!validator.isEmail(email)) {
            this.setState({ error: 'Invalid Email' })
        } else {
            this.setState({ error: '' })
        }
    }

    checkText = (text) => {
        if (validator.isEmpty(text)) {
            this.setState({ error: 'Can not empty' })
        } else if (/\d/g.test(text)) {
            this.setState({ error: 'Can not contain number' })
        } else {
            this.setState({ error: '' })
        }
    }
    checkPassword = (password) => {
        if (!validator.isLength(password, 6)) {
            this.setState({
                error: 'Password should contains minium 6 character',
            })
        } else if (validator.isEmpty(password)) {
            this.setState({ error: 'Password must not be empty' })
        } else {
            this.setState({ error: '' })
        }
    }

    checkTextArea = (text) => {
        if (validator.isEmpty(text)) {
            this.setState({ error: 'Can not empty' })
        } else {
            this.setState({ error: '' })
        }
    }

    handleInput = (event) => {
        if (event.target.name === 'name') {
            this.checkText(event.target.value)
        } else if (event.target.name === 'email') {
            this.checkEmail(event.target.value)
        } else if (event.target.type === 'password') {
            this.checkPassword(event.target.value)
        }
    }
    render() {
        return (
            <React.Fragment>
                <Form.Group key={this.props.input.label}>
                    <Form.Label>{this.props.input.label}</Form.Label>
                    <Form.Control
                        type={this.props.input.inputType}
                        name={this.props.input.name}
                        onChange={this.handleInput}
                    />
                    <p className="visibleError">{this.state.error}</p>
                </Form.Group>
            </React.Fragment>
        )
    }
}

export default InputField
