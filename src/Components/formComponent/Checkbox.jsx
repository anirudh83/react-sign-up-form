import React, { Component } from 'react'
import { Form } from 'react-bootstrap'
import '../Css/style.css'

export class CheckBox extends Component {
    render() {
        let { label, values, name } = { ...this.props.inputs }

        return (
            <div className="checkbox-container">
                <Form.Label>{label}</Form.Label>
                {values.map((value) => (
                    <Form.Check
                        key={value}
                        label={value}
                        name={name}
                        value={value}
                        className="checkbox"
                    />
                ))}

                <p className="visible">{this.props.error}</p>
            </div>
        )
    }
}

export default CheckBox
