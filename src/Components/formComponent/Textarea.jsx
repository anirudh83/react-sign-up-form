import React, { Component } from 'react'
import { Form } from 'react-bootstrap'
import '../Css/style.css'
import validator from 'validator'

export class Textarea extends Component {
    state = { error: '' }
    handleInput = (event) => {
        if (validator.isEmpty(event.target.value)) {
            this.setState({ error: 'Can not empty' })
        } else {
            this.setState({ error: '' })
        }
    }

    render() {
        return (
            <React.Fragment>
                <Form.Label>{this.props.inputs.label}</Form.Label>
                <Form.Control
                    as="textarea"
                    rows="2"
                    type="text"
                    name={this.props.inputs.name}
                    onChange={this.handleInput}
                    style={{
                        background: 'transparent',
                        color: 'white',
                    }}
                    className="textarea"
                />
                <p className="visibleError">{this.state.error}</p>
            </React.Fragment>
        )
    }
}

export default Textarea
