import RadioButton from './formComponent/RadioButton'
import Checkbox from './formComponent/Checkbox'
import DropdownMenu from './formComponent/DropdownMenu'
import InputField from './formComponent/InputField'
import Textarea from './formComponent/Textarea'
import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { submitForm } from './Redux/Index.js'
import './Css/style.css'

export class FormContainer extends Component {
    state = {
        error: '',
    }
    handleValidation = (event) => {
        try {
            if (
                event.target.name.value === '' ||
                event.target.email.value === '' ||
                event.target.password.value === '' ||
                event.target.radioButtonSet.value === '' ||
                event.target.address.value === '' ||
                (event.target.vehicle[0].checked === true ||
                    event.target.vehicle[1].checked === true) === false
            ) {
                this.setState({ error: 'Input fields can not be empty' })
                event.preventDefault()
            } else {
                this.setState({ error: '' })

                let index = 0
                let newStateWithUserInputs = [...this.props.state.userInputs]

                let formValues = [
                    event.target.name.value,
                    event.target.email.value,
                    event.target.password.value,
                    event.target.radioButtonSet.value,
                    {
                        Bike: event.target.vehicle[0].checked === true,
                        Car: event.target.vehicle[1].checked === true,
                    },
                    event.target.vehicle_type.value,
                    event.target.address.value,
                ]

                newStateWithUserInputs.forEach((inputsField) => {
                    inputsField.userInput = formValues[index++]
                })

                this.props.submitForm(newStateWithUserInputs)
            }
        } catch (err) {
            console.log(err)
        }
    }
    render() {
        let userInputs = this.props.state.userInputs

        return (
            <form onSubmit={this.handleValidation}>
                <p className="error-visible-on-top">{this.state.error}</p>

                {userInputs.map((row) => {
                    if (row.inputType === 'radio') {
                        return <RadioButton inputs={row} key={row.label} />
                    } else if (row.inputType === 'checkbox') {
                        return <Checkbox inputs={row} key={row.label} />
                    } else if (row.inputType === 'dropdown') {
                        return <DropdownMenu inputs={row} key={row.label} />
                    } else if (row.inputType === 'textarea') {
                        return <Textarea inputs={row} key={row.label} />
                    } else {
                        return <InputField input={row} key={row.label} />
                    }
                })}

                <Button variant="primary" type="submit" id="submit">
                    Submit
                </Button>
            </form>
        )
    }
}
const mapStateToProps = (state) => {
    return { state: state }
}

const mapDispatchToProps = (dispatch) => {
    return {
        submitForm: (formValues) => dispatch(submitForm(formValues)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer)
